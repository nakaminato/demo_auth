class TopController < ApplicationController
  def index
    @users = User.all.order(id: :asc).page(params[:page])
  end
end
